import { Fragment } from "react";
import Banner from "./../components/Banner";
import carousel1 from "./../images/carousel1-A.jpg";
import carousel2 from "./../images/carousel2-A.jpg";
import carousel3 from "./../images/carousel3-A.jpg";
import {Carousel, Container} from 'react-bootstrap'
import './Pages.css'
import {Row, Col} from 'react-bootstrap'
import grid1 from './../images/grid-img1.jpeg'
import grid2 from './../images/grid-img2.jpeg'
import grid3 from './../images/grid-img3.jpeg'
import grid4 from './../images/grid-img4.jpeg'

export default function Home() {
  const data = {
    title: "Modern and Mindful",
    description: "E-Commerce API",
    destination: "/products",
    buttonDesc: "View Products",
  };
  return (
    <Fragment>
      <Banner bannerProp={data} />
      <Container className="carousel-grp">
        <Carousel>
          <Carousel.Item interval={3000}>
            <img height={450} className="carousel-img"
              className="d-block w-100"
              src={carousel1}
              alt="First slide"
            />
            <Carousel.Caption className="carousel-caption">
              <h1>25% OFF</h1>
              <h3>CREATE YOUR OWN PARADISE</h3>
              <p>BEDROOM COLLECTION</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={3000}>
            <img height={450}  className="carousel-img"
              className="d-block w-100"
              src={carousel2}
              alt="Second slide"
            />
            <Carousel.Caption>
              <h3>TALK ABOUT GOOD BONES</h3>
              <p>LIVING ROOM COLLECTION</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item interval={3000}>
            <img height={450}  className="carousel-img"
              className="d-block w-100"
              src={carousel3}
              alt="Third slide"
            />
            <Carousel.Caption>
              <h3>COLLECT WITH INTENTION</h3>
              <p>
                OFFICE FURNITURE
              </p>
            </Carousel.Caption>
          </Carousel.Item>
        </Carousel>
      </Container>

    <div className="grid-grp">
    <Container>
    <Row className="justify-content-center">
        <Col className="d-flex justify-content-center">
        <img src={grid4} className="grid4"></img>
        </Col>
        <Col>
            <Row className="justify-content-center">
                <Col className="col-grid1">
                <img src={grid1} className="grid1"></img>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col className="col-grid2"> 
                <img src={grid2} className="grid2"></img>
                </Col>
                <Col className="col-grid3">
                <img src={grid3} className="grid3"></img>
                </Col>
            </Row>
        </Col>
    </Row>
    </Container>

    </div>
   
    </Fragment>
  );
}
