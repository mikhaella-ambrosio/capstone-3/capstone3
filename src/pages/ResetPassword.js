import { Container, Row, Col, Form, Button } from "react-bootstrap"
import {useState} from "react"
import "./Pages.css";
const token = localStorage.getItem("token");


export default function ResetPassword(){

    const [email, setEmail] = useState("")
    const [oldPw, setOldPw] = useState("")
    const [pw, setPw] = useState("")
    const [cpw, setCpw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const handleSubmit = (e) => {
        fetch(`https://morning-castle-82091.herokuapp.com/api/users/profile-update`, {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            email: email,
            oldPassword: oldPw, 
            password: pw,
            confirmPw: cpw
          }),
        })
          .then((response) => response.json())
          .then((response) => {
              console.log(response)
            if (response) {
              alert(`You've succesfully updated the product!`);
            }
        });
    };

    return(
        <Container>
        <Row className="justify-content-center align-items-center m-3 p-3">
        <Col id="resetPw-col" xs={10} md={6} className="d-flex justify-content-center">
        <Form id="resetPw-form" className="w-100" onSubmit={(e) => handleSubmit(e)}>
        <h3 className="text-center p-3">RESET PASSWORD</h3>
        <Form.Group className="mb-3">
            <Form.Label>Email Address</Form.Label>
            <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3">
            <Form.Label>Old Password</Form.Label>
            <Form.Control type="password" value={oldPw} onChange={(e) => setOldPw(e.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3">
            <Form.Label>New Password</Form.Label>
            <Form.Control type="password" value={pw} onChange={(e) => setPw(e.target.value)}/>
        </Form.Group>
        <Form.Group className="mb-3">
            <Form.Label>Confirm New Password</Form.Label>
            <Form.Control type="password" value={cpw} onChange={(e) => setCpw(e.target.value)}/>
        </Form.Group>
        <Button id="btn-resetPw" type="submit" disabled = {isDisabled} className="mt-3 p-2 w-100">Reset Password</Button>
    </Form>
        </Col>
    </Row>
    </Container>
    )
}