import { useState, useContext, useEffect } from "react";
import UserContext from "../UserContext";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import './Pages.css'
const token = localStorage.getItem("token");

export default function CreateProduct() {
  const [name, setName] = useState(``);
  const [description, setDescription] = useState(``);
  const [price, setPrice] = useState(0);
  const [isDisabled, setIsDisabled] = useState(true);

  const navigate = useNavigate();
  const { dispatch } = useContext(UserContext);

  useEffect(() => {
    if (token !== null) {
      dispatch({ type: "USER", payload: true });
    }
  }, []);

  const handleSubmit = async (e) => {

    await fetch('https://morning-castle-82091.herokuapp.com/api/products/create', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response) {
          alert(`You've succesfully added a new product!`);

          navigate(`/adminView`);
        }
      });
  };

  return (
    <Container>
      <Row>
        <Col >
          <div className="d-flex justify-content-center" xs={12} md={5}>
            <Form
              id="createProd-form"
              className="w-50 mx-2 my-4 px-2"
              onSubmit={(e) => handleSubmit(e)}
            >
              <h2 className="text-center mb-4 mt-3">ADD A NEW PRODUCT</h2>
              <Form.Group className="mb-3">
                <Form.Label>Product Name:</Form.Label>
                <Form.Control
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Product Description:</Form.Label>
                <Form.Control
                  as="textarea"
                  row={3}
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Price:</Form.Label>
                <Form.Control
                  type="number"
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
              </Form.Group>

              <Button
                type="submit"
                className="createProd-btn w-100 mt-3"
              >
                Submit
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
