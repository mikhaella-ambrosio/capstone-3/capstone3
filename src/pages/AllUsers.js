import { useEffect, useState, Fragment, useContext } from "react";
import { Container, Table, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import UserContext from "./../UserContext";
import "./Pages.css";
const token = localStorage.getItem('token')


export default function AllUsers() {
    const [allUsers, setAllUsers] = useState([]);
    const navigate = useNavigate();
    const { dispatch } = useContext(UserContext);
    
    const fetchData = () => {
        fetch(`https://morning-castle-82091.herokuapp.com/api/users/allUsers`, {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
          .then((response) => response.json())
          .then((response) => {
            console.log(response);
    
            dispatch({ type: "USER", payload: true });
    
            setAllUsers(
              response.map((user) => {
                // console.log(users)
    
                return (
                  <tr key={user._id} className="text-center">
                    <td className="align-middle">{user._id}</td>
                    <td className="align-middle">{user.email}</td>
                    <td className="align-middle">{user.firstName}</td>
                    <td className="align-middle">{user.lastName}</td>
                    <td className="align-middle">
                      {user.isAdmin ? "Admin" : "Non-Admin"}
                    </td>
                    <td>
                      {user.isAdmin ? (
                        <Button
                          className="btn-adminUser "
                          onClick={() => handleDemote(user._id)}
                        >
                          Demote
                        </Button>
                      ) : (
                        <Fragment>
                          <Button
                            className="btn-adminUser "
                            onClick={() => handlePromote(user._id)}
                          >
                            Promote
                          </Button>
                        </Fragment>
                         
                      )}
                      <Button
                         className="btn-adminUser "
                         onClick={() => handleDelete(user._id)}
                       >
                         Delete
                       </Button>
                    </td>
                  </tr>
                );
              })
            );
          });
      };
    
    useEffect(() => {
    fetchData();
    }, []);

    const handlePromote = (userId) => {
        fetch(`https://morning-castle-82091.herokuapp.com/api/users/${userId}/setAsAdmin`, {
          method: "PATCH",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((response) => response.json())
          .then((response) => {
            console.log(response);
    
            if (response) {
              fetchData();
    
              alert("User successfully promoted to admin status.");
            }
          });
      };

    const handleDemote = (userId) => {
    fetch(`https://morning-castle-82091.herokuapp.com/api/users/${userId}/setAsUser`, {
        method: "PATCH",
        headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
    })
        .then((response) => response.json())
        .then((response) => {
        console.log(response);

        if (response) {
            fetchData();

            alert("Admin successfully demoted to user status.");
        }
        });
    };

    const handleDelete = (userId) => {
        console.log();
        fetch(`https://morning-castle-82091.herokuapp.com/api/users/${userId}/delete-user`, {
          method: "DELETE",
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
          },
        })
          .then((response) => response.json())
          .then((response) => {
            console.log(response);
    
            if (response) {
              fetchData();
    
              alert("User successfully Deleted!");
            }
        });
    };

    return (
    <Container className="dashborad-grp">
      <h1 className="mt-5 mb-3 text-center">User Dashboard</h1>
      <div className="dashboard-btn text-right">
        <Link className="btn-admin btn  m-2" to={`/adminView`}>
          Product Dashboard
        </Link>
      </div>
      <Table>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Email</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{allUsers}</tbody>
      </Table>
    </Container>
  );
}
