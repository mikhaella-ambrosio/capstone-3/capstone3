import { useEffect, useState, useContext } from "react";
import { Container, Table, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import UserContext from "./../UserContext";
import "./Pages.css";

export default function Cart() {
    const [totalAmount, setTotalAmount] = useState(0);
    const [purchase, setPurchase] = useState([]);
    const [orders, setOrders] = useState([]);
    const [quantity, setQuantity] = useState(0);

//   useEffect(() => {
//     if (admin === "false") {
//       productsOrdered;
//     } else if (admin === "true") {
//       navigate(`../adminView`);
//     }
//   }, []);

//   const productsOrdered = () => {};

    const handleIncrement = () => {
        setQuantity(quantity + 1)
    }
    const handleDecrement = () => {
        if(quantity > 1) {
        setQuantity(quantity - 1)
        }
    }

  return (
    <Container className="dashborad-grp">
      <h1 className="mt-5 mb-3 text-center">CART </h1>
      <div className="dashboard-btn text-right">
        <Link className="btn-admin btn  m-2" to={`/products`}>
          Add More Items
        </Link>
        <Link className="btn-admin btn  m-2" to={`/allUsers`}>
          Review Payment
        </Link>
      </div>
      <Table>
        <thead>
          <tr className="text-center">
            <th className="align-middle">Product Image</th>
            <th className="align-middle">Product Name</th>
            <th className="align-middle">Price</th>
            <th className="align-middle">Quantity</th>
            <th className="align-middle"><Button className="btn-removeCart">Remove</Button></th>
          </tr>
        </thead>
        <tbody></tbody>
      </Table>
    </Container>
  );
}


{/* <th><Button className='btn-quantityCart' onClick={handleIncrement}>+</Button></th>
            <th>{quantity}</th>
            <th><Button className='btn-quantityCart' onClick={handleDecrement}>-</Button></th> */}