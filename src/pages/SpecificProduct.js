import { useContext, useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { Card, Button, Row, Col, ButtonGroup, Container } from 'react-bootstrap'
import UserContext from "../UserContext";
import image from "./../images/furniture1.jpeg";
import './Pages.css'
const token = localStorage.getItem('token')


export default function SingleProduct(){
    const {state, dispatch} = useContext(UserContext)

    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [price, setPrice] = useState("")
    const [quantity, setQuantity] = useState(1)

    const {productId} = useParams()
    // console.log(useParams())
    const navigate = useNavigate()

    const handleIncrement = () => {
      setQuantity(quantity + 1)
    }
    const handleDecrement = () => {
      if(quantity > 1) {
      setQuantity(quantity - 1)
      }
    }

    const fetchProduct = () => {
      fetch (`https://morning-castle-82091.herokuapp.com/api/products/${productId}`, {
        method: "GET",
        headers:{
          "Authorization": `Bearer ${token}`
        }
      })
      .then(response => response.json())
      .then(response => {
        // console.log(response)
  
        setName(response.name)
        setDescription(response.description)
        setPrice(response.price)
      })
    }

    useEffect(() => {
      if (token !== null) {
        dispatch({ type: "USER", payload: true });
      }
      fetchProduct()
    }, []);

    const handleAddCart = () => {
      fetch(`https://morning-castle-82091.herokuapp.com/api/orders/checkout`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify({
          name: name,
          description: description,
          price: price,
          quantity: quantity,
        })
      })
      .then(response => response.json())
      .then(response => {
        console.log(response)
        if(response) {
          alert(`Item successfully added to cart`)
  
          navigate(`/products`)
        }
      })
    };
    return(

      <Container className="specific-grp">
      <Row className="specific-row justify-content-center">
        <Col xs={12} md={6}>
          <Card className="card-specific m-5">
          <Card.Img className='img-specific' variant="top" src={image} />
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>$ {price}</Card.Text>
              
              <ButtonGroup>
                <Button className='btn-quantity' onClick={handleIncrement}>+</Button>
                <Card.Text className="my-1 mx-3 py-1">{quantity}</Card.Text>
                <Button className='btn-quantity' onClick={handleDecrement}>-</Button>
              </ButtonGroup>
              <div>
              <Button className="btn-specific mt-3 px-3"
                onClick={() => handleAddCart(productId)}>
                Add to Cart
              </Button>
              </div>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
    
    )
}
