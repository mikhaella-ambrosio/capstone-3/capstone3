import { useState, useContext, useEffect } from "react";
import UserContext from "../UserContext";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";
import "./Pages.css";
const token = localStorage.getItem("token");

export default function UpdateProduct() {
  const [name, setName] = useState(``);
  const [description, setDescription] = useState(``);
  const [price, setPrice] = useState(0);
  const { productId } = useParams();

  const navigate = useNavigate();
  const { dispatch } = useContext(UserContext);
console.log(useParams())
const fetchProduct = () => {
    fetch (`https://morning-castle-82091.herokuapp.com/api/products/${productId}`, {
      method: "GET",
      headers:{
        "Authorization": `Bearer ${token}`
      }
    })
    .then(response => response.json())
    .then(response => {
      console.log(response)

      setName(response.name)
      setDescription(response.description)
      setPrice(response.price)
    })
  }

  useEffect(() => {
    if (token !== null) {
      dispatch({ type: "USER", payload: true });
    }
    fetchProduct()
  }, []);

  const handleSubmit = (e) => {
    fetch(`https://morning-castle-82091.herokuapp.com/api/products/${productId}/updateProduct`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
          console.log(response)
        if (response) {
          alert(`You've succesfully updated the product!`);
        }
      });
  };

  return (
    <Container>
      <Row>
        <Col>
          <div className="d-flex justify-content-center" xs={12} md={5}>
            <Form
              id="updateProd-form"
              className="w-50 mx-2 my-4 px-2"
              onSubmit={(e) => handleSubmit(e)}
            >
              <h2 className="text-center mb-4 mt-3">UPDATE PRODUCT</h2>
              <Form.Group className="mb-3">
                <Form.Label>Product Name:</Form.Label>
                <Form.Control
                placeholder={name}
                  type="text"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Product Description:</Form.Label>
                <Form.Control
                  as="textarea"
                  row={3}
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Price:</Form.Label>
                <Form.Control
                  type="number"
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
              </Form.Group>

              <Button type="submit" className="createProd-btn w-100 mt-3">
                Submit
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
