import ProductCard from "./../components/ProductCard";
import {useState, useEffect, Fragment} from "react";
import "./Pages.css";

export default function Product() {
  const [allProducts, setAllProducts] = useState([]);
  
  useEffect(() => {
    const token = localStorage.getItem("token");

    fetch(`https://morning-castle-82091.herokuapp.com/api/products/allProducts`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((result) => result.json())
      .then((result) => {
        console.log(result);
        setAllProducts(
          result.map((product) => {
            return <ProductCard key={product._id} productProp={product} />;
          })
        );
      });
  }, []);

  return (
    <Fragment>
      <div>
        <div id="div-products">
          <a className="cat-products">All</a>
          <a className="cat-products">Living Room</a>
          <a className="cat-products">Kitchen</a>
          <a className="cat-products">Bedroom</a>
          <a className="cat-products">Bathroom</a>
          <a className="cat-products">Outdoor</a>
          <a className="cat-products">Office</a>
        </div>
      </div>
      {allProducts}
    </Fragment>
  );
}
