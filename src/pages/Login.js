import {Row, Col, Form, Button} from "react-bootstrap"
import {useEffect, useState, useContext} from "react"
import { useNavigate } from 'react-router-dom'
import UserContext from './../UserContext'

export default function Login(){
    
    const [email, setEmail] = useState("")
    const [pw, setPw] = useState("")
    const [isDisabled, setIsDisabled] = useState(true)

    const {state, dispatch} = useContext(UserContext)
    const navigate = useNavigate()

    useEffect(() => {
        if(email !== "" && pw !== "" ){
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }
    }, [email, pw])

    const loginUser = async (e) => {
        e.preventDefault()
        // alert(`You have logged in!`)

        await fetch('https://morning-castle-82091.herokuapp.com/api/users/login', {
			method: "POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: pw
			})
		})
        .then(response => response.json())
		.then(response => {
            console.log(response)
            if(response){
			// console.log(response.token)
				localStorage.setItem('token', response.token)
                const token = localStorage.getItem("token")
                // console.log(token)

                fetch('https://morning-castle-82091.herokuapp.com/api/users/profile', {
                    method: "GET",
                    headers:{
                    "Authorization": `Bearer ${token}`}
                })
                .then(response => response.json())
                .then(response => {
                    console.log(response)
                    localStorage.setItem('admin', response.isAdmin)
                    dispatch({type: "USER", payload: true})

                    
                })

                setEmail("")
				setPw("")
                
            } else {
				alert('Incorrect credentials!')
			}
        })
    }
    
    return(

        <Row className="justify-content-center align-items-center m-3 p-3">
            <Col id="login-col"  xs={10} md={6} className="d-flex justify-content-center">
            <Form id="login-form" className="w-100" onSubmit={(e) => loginUser(e)}>
            <h3 className="text-center p-3">LOGIN</h3>
            <Form.Group className="mb-3">
                <Form.Label>Email Address</Form.Label>
                <Form.Control 
                    type="email" 
                    value={email} 
                    onChange = {(e) => setEmail(e.target.value)}
                />
            </Form.Group>
            <Form.Group className="mb-3">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    value={pw}
                    onChange = {(e) => setPw(e.target.value)}
                />
            </Form.Group>
            <a id="reset" href="/resetpassword">Reset Password</a>
            <Button id="btn-login"
            type="submit"
            disabled = {isDisabled}
            className="mt-3 p-2 w-100">
            Login
            </Button>
        </Form>
            </Col>
        </Row>
       
    )
}


//////////////////////

// import {Row, Col, Form, Button} from "react-bootstrap"
// import {useEffect, useState, useContext} from "react"
// import { useNavigate } from 'react-router-dom'
// import UserContext from './../UserContext'

// export default function Login(){
    
//     const [email, setEmail] = useState("")
//     const [pw, setPw] = useState("")
//     const [isDisabled, setIsDisabled] = useState(true)

//     const {state, dispatch} = useContext(UserContext)
//     const navigate = useNavigate()

//     useEffect(() => {
//         if(email !== "" && pw !== "" ){
//             setIsDisabled(false)
//         } else {
//             setIsDisabled(true)
//         }
//     }, [email, pw])

//     const loginUser = async (e) => {
//         e.preventDefault()
//         // alert(`You have logged in!`)

//         await fetch('http://localhost:3026/api/users/login', {
// 			method: "POST",
// 			headers:{
// 				"Content-Type": "application/json"
// 			},
// 			body: JSON.stringify({
// 				email: email,
// 				password: pw
// 			})
// 		})
//         .then(response => response.json())
// 		.then(response => {
//             // console.log(response)
//             if(response){
// 			// console.log(response.token)
// 				localStorage.setItem('token', response.token)
//                 const token = localStorage.getItem("token")
//                 // console.log(token)

//                 fetch('http://localhost:3026/api/users/profile', {
//                     method: "GET",
//                     headers:{
//                     "Authorization": `Bearer ${token}`}
//                 })
//                 .then(response => response.json())
//                 .then(response => {
//                     console.log(response)
//                     localStorage.setItem('admin', response.isAdmin)
//                     dispatch({type: "USER", payload: true})

//                     if(response.isAdmin === false){
//                         dispatch({type:"USER", admin: response.isAdmin});
//                             navigate('/profile');
//                     } else if (response.isAdmin === true){
//                         console.log(`hello`)
//                         dispatch({type:"ADMIN", admin: response.isAdmin});
//                         navigate('/');
//                     }
//                 })

//                 setEmail("")
// 				setPw("")
                
//             } else {
// 				alert('Incorrect credentials!')
// 			}
//         })
//     }
    
//     return(

//         <Row className="justify-content-center align-items-center m-3 p-3">
//             <Col id="login-col"  xs={10} md={6} className="d-flex justify-content-center">
//             <Form id="login-form" className="w-100" onSubmit={(e) => loginUser(e)}>
//             <h3 className="text-center p-3">LOGIN</h3>
//             <Form.Group className="mb-3">
//                 <Form.Label>Email Address</Form.Label>
//                 <Form.Control 
//                     type="email" 
//                     value={email} 
//                     onChange = {(e) => setEmail(e.target.value)}
//                 />
//             </Form.Group>
//             <Form.Group className="mb-3">
//                 <Form.Label>Password</Form.Label>
//                 <Form.Control 
//                     type="password" 
//                     value={pw}
//                     onChange = {(e) => setPw(e.target.value)}
//                 />
//             </Form.Group>
//             <a id="reset" href="/resetpassword">Reset Password</a>
//             <Button id="btn-login"
//             type="submit"
//             disabled = {isDisabled}
//             className="mt-3 p-2 w-100">
//             Login
//             </Button>
//         </Form>
//             </Col>
//         </Row>
       
//     )
// }
