import {Container,Card,ListGroup,Row,Col,Form,Button,} from "react-bootstrap";
import "./Pages.css";


export default function Profile() {
  return (
    <Container>
      <Row>
        <Col md={3} className="my-3 p-3">
          <h3 className="py-3">Welcome Back!</h3>
          <Card>
            <ListGroup variant="flush">
              <ListGroup.Item>Orders</ListGroup.Item>
              <ListGroup.Item>Account Settings</ListGroup.Item>
              <ListGroup.Item>Payments</ListGroup.Item>
              <ListGroup.Item>Addresses</ListGroup.Item>
            </ListGroup>
          </Card>
        </Col>
        <Col md={9} className="my-3 p-3">
          <Row>
            <h4 className="p-3">Account Settings</h4>
          </Row>
          <Row>
            <Col md={6} className="my-3 px-4">
              <p>Update Email Address</p>
              <p>Current Email:</p>
              <div>
                <Form>
                  <Form.Group className="mb-3">
                    <Form.Label>New Email</Form.Label>
                    <Form.Control type="email" />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>Confirm New Email</Form.Label>
                    <Form.Control type="email" />
                  </Form.Group>
                  <Button type="submit" className="btn-profile w-100 mt-2">Apply Changes</Button>
                </Form>
              </div>
            </Col>
            <Col md={6} className="my-3 px-4">
            <p>Update Password</p>
              <div>
                <Form>
                  <Form.Group className="mb-3">
                    <Form.Label>Original Password</Form.Label>
                    <Form.Control type="password" />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>New Password</Form.Label>
                    <Form.Control type="pasword" />
                  </Form.Group>
                  <Form.Group className="mb-3">
                    <Form.Label>Confirm New Password</Form.Label>
                    <Form.Control type="pasword" />
                  </Form.Group>
                  <Button type="submit" className="btn-profile w-100 mt-2">Apply Changes</Button>
                </Form>
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

// {
  /* <Container>
      <Card style={{ width: "18rem" }}>
        <Card.Header>Featured</Card.Header>
        <ListGroup variant="flush">
          <ListGroup.Item>Cras justo odio</ListGroup.Item>
          <ListGroup.Item>Dapibus ac facilisis in</ListGroup.Item>
          <ListGroup.Item>Vestibulum at eros</ListGroup.Item>
        </ListGroup>
      </Card>
    </Container> */
// }
