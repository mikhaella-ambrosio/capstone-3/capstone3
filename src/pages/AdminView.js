import { useEffect, useState, Fragment, useContext } from "react";
import { Container, Table, Button } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import UserContext from "./../UserContext";
import "./Pages.css";

export default function AdminView() {
  const [allProducts, setAllProducts] = useState([]);
  const navigate = useNavigate();
  const { dispatch } = useContext(UserContext);

  const fetchData = () => {
    fetch(`https://morning-castle-82091.herokuapp.com/api/products/allProducts`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);

        dispatch({ type: "USER", payload: true });

        setAllProducts(
          response.map((product) => {
            // console.log(product)

            return (
              <tr key={product._id} className="text-center">
                <td className="align-middle">{product._id}</td>
                <td className="align-middle">{product.name}</td>
                <td className="align-middle">{product.price}</td>
                <td className="align-middle">
                  {product.isActive ? "Active" : "Inactive"}
                </td>
                <td>
                  <Button
                    className="btn-adminProd "
                    onClick={() => handleUpdate(product._id)}
                  >
                    Update
                  </Button>

                  {product.isActive ? (
                    <Button
                      className="btn-adminProd "
                      onClick={() => handleArchive(product._id)}
                    >
                      Archive
                    </Button>
                  ) : (
                    <Fragment>
                      <Button
                        className="btn-adminProd "
                        onClick={() => handleUnarchive(product._id)}
                      >
                        Unarchive
                      </Button>
                      <Button
                        className="btn-adminProd "
                        onClick={() => handleDelete(product._id)}
                      >
                        Delete
                      </Button>
                    </Fragment>
                  )}
                </td>
              </tr>
            );
          })
        );
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleUpdate = (productId) => {
    console.log(productId);
    fetch(`https://morning-castle-82091.herokuapp.com/api/products/${productId}/updateProduct`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);

        if (response) {
          fetchData();

          navigate(`/${productId}/updateProduct`);
        }
      });
  };

  const handleArchive = (productId) => {
    console.log(productId);
    fetch(`https://morning-castle-82091.herokuapp.com/api/products/${productId}/archive`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);

        if (response) {
          fetchData();

          alert("Product successfully archived!");
        }
      });
  };

  const handleUnarchive = (productId) => {
    console.log(productId);
    fetch(`https://morning-castle-82091.herokuapp.com/api/products/${productId}/unarchive`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);

        if (response) {
          fetchData();

          alert("Product successfully unarchived!");
        }
      });
  };

  const handleDelete = (productId) => {
    console.log(productId);
    fetch(`https://morning-castle-82091.herokuapp.com/api/products/${productId}/deleteProduct`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        console.log(response);

        if (response) {
          fetchData();

          alert("Product successfully Deleted!");
        }
      });
  };

  return (
    <Container className="dashborad-grp">
      <h1 className="mt-5 mb-3 text-center">Product Dashboard</h1>
      <div className="dashboard-btn text-right">
        <Link className="btn-admin btn  m-2" to={`/createProduct`}>
          Add Product
        </Link>
        <Link className="btn-admin btn  m-2" to={`/allUsers`}>
          User Dashboard
        </Link>
      </div>
      <Table>
        <thead>
          <tr className="text-center">
            <th>ID</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Status</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{allProducts}</tbody>
      </Table>
    </Container>
  );
}
