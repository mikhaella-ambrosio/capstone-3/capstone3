import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import 'font-awesome/css/font-awesome.min.css'
import "./index.css";


ReactDOM.render(
  <Fragment>
    <App/>
  </Fragment>,
  document.getElementById('root')
);

