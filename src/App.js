import {
  Routes,
  Route,
  BrowserRouter,
} from "react-router-dom";
import { UserProvider } from "./UserContext"
import { useReducer } from "react";
import { initialState, reducer } from './reducer/UserReducer'

import Home from "./pages/Home"
import Register from "./pages/Register"
import Login from "./pages/Login"
import Products from "./pages/Products"
import AppNavbar from "./components/AppNavbar";
import Footer from "./components/Footer";
import Cart from "./pages/Cart";
import ErrorPage from "./pages/ErrorPage";
import Logout from "./pages/Logout";
import CreateProduct from "./pages/CreateProduct"
import SpecificProduct from "./pages/SpecificProduct";
import Profile from "./pages/Profile";
import AdminView from "./pages/AdminView";
import ResetPassword from "./pages/ResetPassword";
import UpdateProduct from './pages/UpdateProduct'
import AllUsers from "./pages/AllUsers";


function App(){

  const [state, dispatch] = useReducer(reducer, initialState)
  // console.log(state)

  return(
    <UserProvider value={{state,dispatch}}>
      <BrowserRouter>
      <AppNavbar/>
      <Routes>
        <Route path="/" element={ <Home/> }/>
        <Route path="/products" element={ <Products/> }/>
        <Route path="/register" element={ <Register/> }/>
        <Route path="/login" element={ <Login/> }/>
        <Route path="/cart" element={ <Cart/> }/>
        <Route path="/logout" element={ <Logout/> }/>
        <Route path="/profile" element={ <Profile/> }/>
        <Route path="/adminView" element={ <AdminView/> }/>
        <Route path="/resetPassword" element={ <ResetPassword/> }/>
        <Route path="/allUsers" element={ <AllUsers/> }/>

        <Route path="/createProduct" element={ <CreateProduct/> }/>
        <Route path="/products/:productId" element={ <SpecificProduct/> } />
        <Route path="/:productId/updateProduct" element={ <UpdateProduct/> } />

        <Route path="*" element={ <ErrorPage/> }/>
      </Routes>
      <Footer/>
      </BrowserRouter>
    </UserProvider> 
  )
}

export default App;

