import { Col, Row } from "react-bootstrap"

export default function Banner({bannerProp}){
    console.log(bannerProp)
    const {title, description, destination, buttonDesc} = bannerProp

  return (
     <div id="banner-bg" className="jumbotron jumbotron-fluid ">
        <Row className="justify-content-around">
            <Col md={5}className="d-none d-sm-none d-md-block  mx-0 px-0">
                <img id="banner-img" className="img-fluid" src="https://images.pexels.com/photos/545034/pexels-photo-545034.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2"></img>
            </Col>
            <Col md={5} id="banner-text">
                <div className="container">
                    <h1 className="display-4">{title}</h1>
                    <p className="lead">{description}</p>
                    <a className="btn btn-info" href={destination}>{buttonDesc}</a>
                </div>
            </Col>
        </Row>
        
	</div>
  )
}


{/* <Carousel variant="dark" className="text-center">
        <Carousel.Item interval={1000} >
            <img fluid 
            className="d-flex w-75 h-25 justify-content-center"
            src={require("./../images/carousel1-A.jpg")}
            alt="First slide"
            />
            <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={1000}>
            <img fluid
            className="d-block w-75 h-25"
            src={require("./../images/carousel2-A.jpg")}
            alt="Second slide"
            />
            <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item interval={1000} >
            <img fluid
            className="d-block w-75 h-25"
            src={require("./../images/carousel3-A.jpg")}
            alt="Third slide"
            />
            <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </Carousel.Caption>
        </Carousel.Item>
    </Carousel> */}