import { Row, Col } from "react-bootstrap";
export default function Footer() {
  return (
      <div id="footer" className="fixed-bottom">
        <Row id="footer-grp">
          <Col md={4}>
            <h6>Contact Us</h6>
            <p>Phone: 09693140133</p>
            <p>Mon - Fri: 8 am -7 pm PST</p>
          </Col>
          <Col md={4}>
            <h6>Order Tracking</h6>
            <p>Find out when your online purchase will arrive.</p>
            <p>Track Your Order</p>
          </Col>
        </Row>
      </div>
  );
}

{
  /* <div id="footer" className="d-flex justify-content-center align-items-center" style={{height: `10vh`}}>
        <p className="m-0">Mikhaella &#64; E-Commerce API | Zuitt Coding Bootcamp &#169;</p>
        </div> */
}
