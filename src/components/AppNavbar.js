import { Fragment, useContext } from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import UserContext from "./../UserContext";
import "./Components.css";


export default function AppNavBar() {
  const { state, dispatch } = useContext(UserContext);
  console.log(state);

  const token = localStorage.getItem("token");
const admin = localStorage.getItem("admin");
	// const AdminUser = () => {
	// 	if (admin === false) {
	// 		return (
	// 			<Fragment>
	// 				<Nav.Link id="navlink" href="/products">Products</Nav.Link>
	// 				<Nav.Link id="navlink" href="/cart">Cart</Nav.Link>
	// 				<Nav.Link id="navlink" href="/profile">Profile</Nav.Link>
	// 			</Fragment>
	// 		)
	// 	} 
	// 	if (admin === true) {
	// 		return (
	// 			<Fragment>
	// 				<Nav.Link id="navlink" href="/adminView">Dashboard</Nav.Link>
	// 			</Fragment>
	// 		)
	// 	}
	// }
	const NavLinks = () => {
		if (token === null) {
			return (
				<Fragment>
					<Nav.Link id="navlink" href="/login">Login</Nav.Link>
					<Nav.Link id="navlink" href="/register">Register</Nav.Link>
				</Fragment>
			)
		} else {
			return (	
				<Fragment>
					{/* <AdminUser /> */}
					<Nav.Link id="navlink" href="/logout">Logout</Nav.Link>
				</Fragment>
			)
		}
	}
  return (
    <Navbar id="navbar-bg" expand="lg" sticky="top">
      <Container>
        <Navbar.Brand id="web-logo" href="/">
          Adhika
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav>
		  <Nav.Link id="navlink" href="/">Home</Nav.Link>
		  {
			admin === false
			? (
				<Nav.Link id="navlink" href="/adminView">Dashboard</Nav.Link>
				
  			) : ( 
				<Fragment>
					<Nav.Link id="navlink" href="/adminView">Dashboard</Nav.Link>
				<Nav.Link id="navlink" href="/products">Products</Nav.Link>
				<Nav.Link id="navlink" href="/cart">Cart</Nav.Link>
				<Nav.Link id="navlink" href="/profile">Profile</Nav.Link>
				</Fragment>
			  )
		  }
		  
			<NavLinks />
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

/////////

// <Fragment>
// 	<Nav.Link id="navlink" href="/products">Products</Nav.Link>
// 	<Nav.Link id="navlink" href="/cart">Cart</Nav.Link>
// 	<Nav.Link id="navlink" href="/profile">Profile</Nav.Link>
// </Fragment>
// : 
// <Fragment>
// 	<Nav.Link id="navlink" href="/adminView">Dashboard</Nav.Link>
// </Fragment>

