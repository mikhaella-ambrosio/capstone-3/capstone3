import { Container, Card, Button, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import image from "./../images/furniture1.jpeg";
import './Components.css'

export default function ProductCard({ productProp }) {
  const { _id, name, description, price } = productProp;

  // add to cart button
  const handleClick = () => {
    
    fetch("https://morning-castle-82091.herokuapp.com/api/orders/addToCart", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name,
        description: description,
        price: price,
      }),
    });
  };

  return (
    <Link className="productCard-Link" to={`/products/${_id}`}>
      <Card className="productCard">
        <Card.Body className="card-body text-right">
          <Card.Img className="img-prodCard img-fluid" variant="top" src={image} />
          <Card.Title className="card-title">{name}</Card.Title>
          <Card.Text>
            <p>{description}</p>
          </Card.Text>
          <Card.Text className="price-prodCard">
            <p>$ {price}</p>
          </Card.Text>
          <Button className="btn-prodCard" size="md" onClick={handleClick}>
            View Item
          </Button>
        </Card.Body>
      </Card>
    </Link>
  );
}
